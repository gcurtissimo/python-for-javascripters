class WeirdThing:
    __slots__ = ("_values")  # ("foo")

    def __init__(self):
        self._values = {}

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            key = (key.start, key.stop, key.step)
        self._values[key] = value 

    def __getattr__(self, name):
        if name == "_values":
            return getattr(self, "_values")
        return self._values.get(name, None)

    def __add__(self, other):
        return "A thing!"

    def __lt__(self, other):
        return "WHAT?????"

    def __len__(self):
        return 100000000000

a = [1, 2, 3, 4]
print(len(a))

thing = WeirdThing()
print(thing + None)
print(thing < 10000)
print(len(thing))

x = thing.whatever
print("x", x)
thing["whatever"] = "FOOLS!"
print(thing.whatever)


print(thing)
thing["foo"] = 7
thing[1:2:3] = 4
thing[1, 2, 3, 4] = 5
thing[1, 2, 3] = "WHOA!"

from pprint import pprint
pprint(thing._values)
# print(thing.foo)
