# Python for JavaScripters Presentation

For those of you interested in looking at the artifacts in this
presentation, you should install JupyterLab or Jupyter Notebooks.

You can find that at [Jupyter](https://jupyter.org/)

Otherwise, all you really need is Python 3. ;-)
